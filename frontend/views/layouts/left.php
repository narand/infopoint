<aside class="main-sidebar">

	<section class="sidebar">

<?php

echo dmstr\widgets\Menu::widget([
    'options' => [
        'class' => 'sidebar-menu'
    ],
    'items' => [
        [
            'label' => 'Email',
            'icon' => 'fa fa-envelope',
            'url' => [
                '/email'
            ]
        ]

    ]
])
?>

    </section>

</aside>
