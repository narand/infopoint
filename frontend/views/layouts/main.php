<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') {
/**
 * Do not use this code in your template. Remove it.
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
/*
    $this->registerCssFile('/files/fa/css/font-awesome.min.css', [
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset'
        ]
    ]);
*/
    $this->registerCssFile('/files/ff/jquery.fancybox.css', [
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset'
        ]
    ]);

    $this->registerJsFile('/files/ff/jquery.fancybox.pack.js', [
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset'
        ]
    ]);

    $this->registerJsFile('/files/ff/jquery.mousewheel-3.0.6.pack.js', [
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset'
        ]
    ]);


    $this->registerCssFile('/files/ionicons/css/ionicons.min.css', [//
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset'
        ]
    ]);

    $this->registerJsFile('/files/animo.min.js', [
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset'
        ]
    ]);

    $this->registerCssFile('/files/animate.min.css', [//
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset'
        ]
    ]);

    $this->registerCssFile('/site/site.min.css?'.date('U'), [
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset'
        ]
    ]);

    $this->registerJsFile('/site/site.min.js?'.date('U'), [
        'depends' => [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset'
        ]
    ]);

    $this->registerJs('$(".fancybox").fancybox();');


    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition <?= \dmstr\helpers\AdminLteHelper::skinClass() ?> sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => '<br>'.$content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
