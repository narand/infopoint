<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Email */

$this->title = 'Редактирование сообщения: ' . $model->subject;
$this->params['breadcrumbs'][] = [
    'label' => 'Сообщения',
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = [
    'label' => $model->subject,
    'url' => [
        'view',
        'id' => $model->id
    ]
];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="email-update">
    <?=$this->render('_form', ['model' => $model])?>

</div>
