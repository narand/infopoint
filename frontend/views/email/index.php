<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EmailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title='Сообщения';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="email-index">
    <p>
        <?= Html::a('Создать новое сообщение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['attribute' => 'email','options'=>['width'=>'250']],
            ['attribute' => 'date','options'=>['width'=>'150']],
            ['attribute' => 'subject'],
            ['attribute' => 'status','options'=>['width'=>'150'],
                'filter' => ["0" => "Не высланное","1" => "Высланное"],
                'content'=>function ($data) {return $data->status!=0?'Высланное':'Не высланное';}

        ],
             //'message:ntext',
            ['class' => 'yii\grid\ActionColumn','options'=>['width'=>'80']],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
