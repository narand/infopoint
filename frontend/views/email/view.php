<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\widgets\Growl;

if ($send) {
        Growl::widget([
            'type' => Growl::TYPE_SUCCESS,
            'title' => 'Информация',
            'icon' => 'glyphicon glyphicon-ok-sign',
            'body' => 'Сообщение выслано.',
            'showSeparator' => true,
            'delay' => 0,
            'pluginOptions' => [
                'showProgressbar' => true,
                'placement' => [
                    'from' => 'top',
                    'align' => 'center',
                ]
            ]
        ]);
}

$this->title = $model->subject;
$this->params['breadcrumbs'][] = [
    'label' => 'Сообщения',
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-view">
	<div class="pull-left">

        <?=Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=Html::a('Удалить', ['delete','id' => $model->id], ['class' => 'btn btn-danger','data' => ['confirm' => 'Вы уверены?','method' => 'post']])?>
        </div>
	<div class="pull-right">
	     <?php
    ActiveForm::begin();
    echo Html::hiddenInput('send','!').Html::submitButton('Выслать', [
        'class' => 'btn btn-info'
    ]);
    ActiveForm::end();
    ?>
    </div>

    <?=DetailView::widget(['model' => $model,'attributes' => ['date','subject:ntext','message:ntext','email:ntext']])?>

</div>
<hr>
<b>Прикреплённых файлов: <?php
echo Yii::$app->db->createCommand('SELECT COUNT(*) FROM `attachment` WHERE email_id=:email_id')
    ->bindValue(':email_id', $model->id)
    ->queryScalar()?></b>
<hr>
<div style="text-align: center">
<?php
ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]);
echo FileInput::widget([
    'name' => 'files[]',
    'options' => [
        'multiple' => true,
        'accept' => '*/*'
    ],
    'pluginOptions' => [
        'showPreview' => true,
        'showCaption' => true,
        'showRemove' => true,
        'showUpload' => true,
        'browseLabel' => 'Прикрепить файлы',
        'maxFileCount' => 10
    ]
]);

ActiveForm::end();
?></div>
