<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Email */

$this->title = 'Создание сообщения';
$this->params['breadcrumbs'][] = ['label' => 'Сообщения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
