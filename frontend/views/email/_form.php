<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model frontend\models\Email */
/* @var $form yii\widgets\ActiveForm */
?>
    <?php $form = ActiveForm::begin(); ?>

<div class="row">

	<div class="col-md-6">
    <?= $form->field($model, 'subject')->textInput() ?>
</div>
	<div class="col-md-6">
    <?= $form->field($model, 'email')->textInput() ?>
</div>
	<div class="col-md-12">
    <?=$form->field($model, 'message')->widget(CKEditor::className(), ['editorOptions' => ['preset' => 'full','inline' => false,'rows' => 6]])?>
</div>
</div>
<div style="text-align: center">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
