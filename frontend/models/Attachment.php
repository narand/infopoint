<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "attachment".
 *
 * @property integer $id
 * @property integer $email_id
 * @property string $name
 * @property string $original
 *
 * @property Email $email
 */
class Attachment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email_id', 'name', 'original'], 'required'],
            [['email_id'], 'integer'],
            [['name', 'original'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email_id' => 'Email ID',
            'name' => 'Name',
            'original' => 'Original',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmail()
    {
        return $this->hasOne(Email::className(), ['id' => 'email_id']);
    }
}
