<?php
namespace frontend\models;

use Yii;

/**
 * This is the model class for table "email".
 *
 * @property integer $id
 * @property string $date
 * @property integer $status
 * @property string $subject
 * @property string $message
 * @property string $email
 *
 * @property Attachment $id0
 */
class Email extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'message', 'email'], 'required'],
            ['email','email'

            ],
            [
                'date',
                'default',
                'value' => date('Y-m-d H:i:s')
            ],
            [
                'status',
                'default',
                'value' => 0
            ],
            [['date'], 'safe'],
            [['status'], 'integer'],
            [['subject', 'message', 'email'], 'string'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Attachment::className(), 'targetAttribute' => ['id' => 'email_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'status' => 'Статус',
            'subject' => 'Тема',
            'message' => 'Сообщение',
            'email' => 'Адресат'
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Attachment::className(), [
            'email_id' => 'id'
        ]);
    }
}
