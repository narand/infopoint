<?php
namespace frontend\controllers;

use Yii;
use frontend\models\Email;
use frontend\models\EmailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use PHPMailer\PHPMailer\PHPMailer;
use frontend\models\Attachment;

/**
 * EmailController implements the CRUD actions for Email model.
 */
class EmailController extends Controller
{

    /**
     * Creates a new Email model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => [
                        'POST'
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Email models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 10;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Email model.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $send = 0;
        if (isset($_POST['send'])) {
            $email = Email::find()->where('id=:id', [
                ':id' => $id
            ])->one();
            if ($email) {
                $mail = new PHPMailer();
                $mail->isHTML(true);
                $mail->From = 'noreply@topcheapcar.com';
                $mail->Subject = $email['subject'];
                $mail->Body = $email['message'];
                $mail->AddAddress($email['email']);

                foreach (Attachment::find()->where('email_id=:id', [
                    ':id' => $id
                ])->all() as $attachment) {
                    $mail->AddAttachment('upload/' . $attachment['name'], $attachment['original']);
                }

                $mail->Send();

                Yii::$app->db->createCommand('UPDATE email SET status=1 WHERE id=:id LIMIT 1')
                    ->bindValue(':id', $id)
                    ->execute();
                $send = 1;
            }
        }

        if ($_FILES) {
            for ($i = 0; $i < 10; $i ++) {

                if (isset($_FILES['files']['name'][$i])) {
                    $file = md5(date('U') . $_FILES['files']['name'][$i] . $i . $_FILES['files']['tmp_name'][$i]);
                    move_uploaded_file($_FILES['files']['tmp_name'][$i], 'upload/' . $file);
                    Yii::$app->db->createCommand('INSERT INTO `attachment` VALUES (NULL, :email_id, :name, :original)')
                        ->bindValue(':email_id', $id)
                        ->bindValue(':name', $file)
                        ->bindValue(':original', $_FILES['files']['name'][$i])
                        ->execute();
                }
            }
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'send' => $send
        ]);
    }

    /**
     * Creates a new Email model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Email();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing Email model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing Email model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect([
            'index'
        ]);
    }

    /**
     * Finds the Email model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Email the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Email::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
